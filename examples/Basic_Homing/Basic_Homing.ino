/*
	Basic_Homing.ino

	Basic example for controlling Zaber devices in the binary protocol.

	Sends a Zaber device to its home position on startup.
*/

#include <ZaberBinary.h>
 
ZaberBinary zb(Serial);
 
void setup() {
 
	/* Initialize baudrate to 9600, typical for Zaber binary devices. */
	Serial.begin(9600);
 
	/* Issue a home command to device 1. */
	zb.send(1, 1, 0);

	/* Always consume the reply to a command, even if not checking it for errors. */
	/* Note that in the Binary protocol, move commands reply when the move finishes. */
	zb.receive();
 
	/* Additional commands can now be issued. */
}
 
void loop() {
 
}
