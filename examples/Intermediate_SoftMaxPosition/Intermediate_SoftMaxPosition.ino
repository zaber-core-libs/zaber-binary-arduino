/*
	Intermediate_SoftMaxPosition.ino

	Intermediate example for controlling Zaber devices in the binary protocol.

	Specifies a soft-maximum position limit for the Zaber device, then polls for the device's position
	on each loop. If the device's position reaches over the maximum position defined, the device is
	sent to its home position.
*/
#include <ZaberBinary.h>

int SOFT_MAX = 6000;
 
ZaberBinary zb(Serial);
 
void setup() {
	/* Initialize baudrate to 9600, typical for Zaber binary devices. */
	Serial.begin(9600);
}
 
void loop() {
	/* Issue a get position command to device 1 (this number may need to be modified). */
	zb.send(1, ZaberBinary::Command::GET_POS, 0);

	/* Receive the command from the device. */
	ZaberBinary::reply posResponse = zb.receive();

	/* Check that the response is from the GET POS command. */
	if(posResponse.commandNumber == ZaberBinary::Command::GET_POS)
	{
		/* If the position returned is greater than SOFT_MAX, we'll send the device home. */
		if(posResponse.responseData > SOFT_MAX)
		{
			/* Issue a home command to the device. */
			zb.send(1, ZaberBinary::Command::HOME, 0);

			/* Block requests on the device until it is idle. */
			/* Note this consumes the reply to the home command. */
			zb.pollUntilIdle(1);
		}
	}
}