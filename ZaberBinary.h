/*
	ZaberBinary.h - Binary Library for interacting with Zaber devices
	January 9, 2018.
*/

#ifndef ZaberBinary_h
#define ZaberBinary_h

#include <Arduino.h>

class ZaberBinary
{
	public:
	
		/* 	
			Contains hard-coded example command number constants
			See: https://www.zaber.com/wiki/Manuals/Binary_Protocol_Manual for more commands.
		*/
		class Command 
		{
			public:

				enum command : byte 
				{ 
					HOME = 1,
					MOVE_ABS = 20,
					MOVE_REL = 21,
					STOP = 23,
					GET_POS = 60,
					GET_FW = 50,
					GET_STATUS = 54
				};
		};
	
		/*
			@brief Initializes the a ZaberBinary class instance on the specified serial port.
			
			@param serial 	The serial class to use (Eg: Serial, Serial1, ...)
		*/
		ZaberBinary(Stream& serial);
		
		/* reply struct format returned when a command is finished executing */
		struct reply 
		{
			/* Evaluates to the device number the reply is in relation to */
			int deviceNumber;
			/* Evaluates to the command number the reply is in relation to */
			int commandNumber;
			/* The attached reply data in unsigned long format */
			unsigned long responseData;
			/* Evaluates to true if the issuing of the command resulted in an error */
			bool isError;	
		}; 
		
		/*
			@brief Returns true if the device has a response of size 6 bytes to read (as Zaber devices respond with
			6 byte replies), false otherwise.
		*/
		bool isReplyAvailable();
		
		/*
			@brief Stops execution of the current program until a reply has been read, and returns the reply. This is 
			used to wait until the device has finished successfully executing one command before executing the other.
		*/
		struct reply receive();
		
		/*
			@brief Issues a command of the specified index to the specified device,	with the specified data. Note that 
			multi-axis devices appear as multiple devices with their own device numbers. For more information, see:
			
				https://www.zaber.com/wiki/Manuals/Binary_Protocol_Manual#Message_Format
			
			@param device 	The device number to send the command to (0 = all devices)
		*/
		void send(byte device, byte commandNumber, long data);

		/*
			@brief Blocks program execution until the specified device has ceased moving. Use with caution as
			this function will discard the reply to the original command and will also discard replies from
			other devices if more than one is operating at the same time.
			
			@param device 	The device number upon which to poll until idle
		*/
		void pollUntilIdle(int device);

		/*
			@brief Drains the serial port of all messages.
		*/
		void drain();

	private:

		/* Reference to the serial class in use */
		Stream& _serialPort;

		/*
			@brief Checks to see whether a reply has been sent from the device, and if so, returns a reply.
		*/
		struct reply readSerialPortReply();
};

#endif