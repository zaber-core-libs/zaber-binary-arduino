# Changelog

## v1.1.1 (2018-09)

* ZaberBinary::drain() now clears the receive buffer of incomplete as well as complete messages.
* Removed some superfluous include statements that caused problems on Linux.
* Added comments about consuming replies to example programs.

## v1.1 (2018-05)

* Updated link to user guide.

## v1.0 (2018-03)

Initial release