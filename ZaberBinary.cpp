/*
	ZaberBinary.cpp - Implements the binary library for interacting with Zaber devices
	January 11, 2018
	See ZaberBinary.h for documentation
*/

#include "ZaberBinary.h"

ZaberBinary::ZaberBinary(Stream& serial):
	_serialPort(serial)
{
	/* An empty constructor is required in order to correctly initialize the serial reference before the body */
}

struct ZaberBinary::reply ZaberBinary::receive()
{  
	/* Delay until the appropriate-size byte reply is read */
	while(_serialPort.available() < 6) 
	{
		delay(10);
	}

	/* Decode the reply into a reply struct and return it to the user */
	reply reply = readSerialPortReply();
	return reply;
}

bool ZaberBinary::isReplyAvailable() 
{
	/* Delay until the appropriate-size byte reply is read */
	return (_serialPort.available() >= 6);
}

void ZaberBinary::send(byte device, byte commandNumber, long data)
{	
	/* Convert the data into 4 separate bytes */
	byte byte1 = (data & 0xff000000UL) >> 24;
	byte byte2 = (data & 0x00ff0000UL) >> 16;
	byte byte3 = (data & 0x0000ff00UL) >> 8;
	byte byte4 = (data & 0x000000ffUL);
	
	/* Issue the command to the device with the converted data */
	byte instruction[6] = {device, commandNumber, byte4, byte3, byte2, byte1};
	_serialPort.write(instruction, 6);
}

struct ZaberBinary::reply ZaberBinary::readSerialPortReply()
{
	/* Initialize all bytes to 0 to prevent overflow or accidental overlaps */
	byte replyBytes[6] = { 0, 0, 0, 0, 0, 0 };
	
	/* When a reply of the appropriate byte-size is seen, load it into an array for decoding */
	if(_serialPort.available() > 5)
	{
		for(int i = 0; i < 6; i++)
		{
			replyBytes[i] = _serialPort.read();
		}
	}
	
	/* Form and return a reply struct with the read properties */
	int deviceNumber = replyBytes[0];
	int commandNumber = replyBytes[1];
		
	/* Convert the reply bytes back into readable data */
	unsigned long responseData = (unsigned long) (replyBytes[2] + 256UL*replyBytes[3] + 65536UL*replyBytes[4]
		+ 16777216UL*replyBytes[5]);
		
	bool isError = (commandNumber == 255);
	
	reply result = { deviceNumber, commandNumber, responseData, isError};
	return result;
}

void ZaberBinary::pollUntilIdle(int device)
{
	/* Delay in 10 millisecond increments until the device is no longer busy */
	send(device, ZaberBinary::Command::GET_STATUS, 0);
	ZaberBinary::reply resp = receive();

	while((resp.commandNumber != ZaberBinary::Command::GET_STATUS)
		  || (resp.deviceNumber != device)
		  || ((resp.responseData != 0) && (resp.responseData != 65)))
	{
		delay(10);
		if (!isReplyAvailable())
		{
			send(device, ZaberBinary::Command::GET_STATUS, 0);
		}

		resp = receive();
	}
}

void ZaberBinary::drain()
{
	while(_serialPort.available() > 0)
	{
		_serialPort.read();
	}
}