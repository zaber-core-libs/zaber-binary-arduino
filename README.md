# Zaber Binary Arduino Library

Provides implementation for Arduino boards to interact with Zaber devices in the binary protocol over serial communications.

## Getting Started Guide

You can get up and running with the following Arduino code to send your device to its home position.

```cpp
#include <ZaberBinary.h>

ZaberBinary zb(Serial);
 
void setup() {
 
    /* Initialize baudrate to 9600, typical for Zaber binary devices */
    Serial.begin(9600);
 
    /* Issue a home command to device 1 */
    zb.send(1, 1, 0);
}
 
void loop() {
 
}
```

An additional beginner's guide to getting started is available [here](#) on the Zaber wiki.

## Examples

Arduino `.ino` examples are available in the `examples` directory.

## Documentation

See `ZaberBinary.h`.